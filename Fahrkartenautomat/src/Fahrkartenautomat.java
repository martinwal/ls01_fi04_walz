﻿import java.util.Scanner;

class Fahrkartenautomat {

	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		String[] ticketBezeichnungen = new String[] { "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC",
				"Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC",
				"Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC" };
		double[] ticketPreise = new double[] { 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90 };
		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin aus:\n");
		for (int i = 0; i < 10; i++) {
			System.out.printf("[%1$2d] \"%2$s\" Preis: %3$.2f EURO\n", i + 1, ticketBezeichnungen[i], ticketPreise[i]);
		}

		int ticketTyp = 0;
		double zuZahlenderBetrag = 0;
		while (ticketTyp < 1 || ticketTyp > ticketPreise.length) {
			System.out.print("\nIhre Wahl: ");
			ticketTyp = tastatur.nextInt();
	
			if (ticketTyp < 1 || ticketTyp > ticketPreise.length)
				System.out.println(">> falsche Eingabe <<");
			else
				zuZahlenderBetrag = ticketPreise[ticketTyp - 1];
		}

		int ticketAnzahl = 0;
		while (ticketAnzahl < 1 || ticketAnzahl > 10) {
			System.out.print("Anzahl der Fahrscheine: ");
			ticketAnzahl = tastatur.nextInt();

			if (ticketAnzahl < 1 || ticketAnzahl > 10) {
				System.out.println(
						"Die eingegebene Fahrkartenanzahl ist nicht gültig. Bitte geben Sie einen Wert von 1 bis 10 an!");
			}
		}

		zuZahlenderBetrag = zuZahlenderBetrag * ticketAnzahl;
		return zuZahlenderBetrag;
	}

	public static double fahrkartenBezahlen(double zuZahlen) {
		Scanner tastatur = new Scanner(System.in);
		double eingeworfeneMünze = 0;
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlen) {
			System.out.printf("Noch zu zahlen: %.2f EURO\n", (zuZahlen - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 EURO): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlen;

		return rückgabebetrag;
	}

	public static void fahrkartenAusgeben() {
		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double rückgabebetrag) {
		// Rückgeldberechnung und -Ausgabe
		// -------------------------------
		if (rückgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rückgabebetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2, "EURO");
				// System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				muenzeAusgeben(1, "EURO");
				// System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (Math.round(rückgabebetrag * 100.0) / 100.0 >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgeben(50, "CENT");
				// System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (Math.round(rückgabebetrag * 100.0) / 100.0 >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgeben(20, "CENT");
				// System.out.println("20 CENT");
				rückgabebetrag -= 0.2;

			}
			while (Math.round(rückgabebetrag * 100.0) / 100.0 >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10, "CENT");
				// System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (Math.round(rückgabebetrag * 100.0) / 100.0 >= 0.05)// 5 CENT-Münzen
			{
				muenzeAusgeben(5, "CENT");
				// System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}
	}

	public static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);
	}

	public static void main(String[] args) {
		while (true) {
			double rückgabebetrag;
			double zuZahlenderBetrag;
			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

			fahrkartenAusgeben();
			rueckgeldAusgeben(rückgabebetrag);

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.");
			System.out.println("---");
		}
	}
}